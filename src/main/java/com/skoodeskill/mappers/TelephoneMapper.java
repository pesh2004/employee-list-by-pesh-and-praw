package com.skoodeskill.mappers;

import com.skoodeskill.model.Telephone;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
 * Created by PeshZ on 10/8/15.
 */
public interface TelephoneMapper {

    @Insert("insert into telephone(employee_id,phone_number)values(#{employeeId},#{phoneNumber})")
    @Options(useGeneratedKeys = true, keyProperty = "id", keyColumn = "id")
    public int insertTelephone(Telephone telephone);

    @Select("select * from telephone where employee_id = #{employeeId}")
    @Results({
            @Result(id=true, property = "id",column = "id"),
            @Result(property = "employeeId", column = "employee_id"),
            @Result(property = "phoneNumber", column = "phone_number")
    })
    public List<Telephone> getTelephoneByEmployeeId(int employeeId);

    @Delete("delete from telephone where employee_id = #{employeeId}")
    public int deleteTelephoneByEmployeeId(int employeeId);
}
