package com.skoodeskill.mappers;

import com.skoodeskill.model.Email;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
 * Created by PeshZ on 10/8/15.
 */
public interface EmailMapper {
    @Insert("insert into email(employee_id,email)values(#{employeeId},#{email})")
    @Options(useGeneratedKeys = true, keyProperty = "id", keyColumn = "id")
    public int insertEmail(Email email);

    @Select("select * from email where employee_id = #{employeeId}")
    @Results({
            @Result(id=true, property = "id",column = "id"),
            @Result(property = "employeeId", column = "employee_id"),
            @Result(property = "email", column = "email")
    })
    public List<Email> getEmailByEmployeeId(int employeeId);

    @Delete("delete from email where employee_id = #{employeeId}")
    public int deleteEmailByEmployeeId(int employeeId);
}
