package com.skoodeskill.mappers;

import com.skoodeskill.model.Employee;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
 * Created by PeshZ on 10/7/15.
 */
public interface EmployeeMapper {
    @Select("select * from employee")
    @Results({
            @Result(id=true, property = "id",column = "id"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "nickName", column = "nick_name"),
            @Result(property = "gender", column = "gender"),
            @Result(property = "dob", column = "dob"),
            @Result(property = "department", column = "department_id",one = @One(select="com.skoodeskill.mappers.DepartmentMapper.getDepartmentById")),
            @Result(property = "salary", column = "salary"),
            @Result(property = "status", column = "status"),
            @Result(property = "photo", column = "photo")
    })
    public List<Employee> getAllEmployee();

    @Insert("insert into employee(first_name,last_name,nick_name,gender,dob,department_id,salary,status,photo)values(#{firstName},#{lastName},#{nickName},#{gender},#{dob},#{department.id},#{salary},#{status},#{photo})")
    @Options(useGeneratedKeys = true, keyProperty = "id", keyColumn = "id")
    public int insertEmployee(Employee employee);

    @Update("update employee set first_name = #{firstName}, last_name = #{lastName}, nick_name = #{nickName}, gender = #{gender}, dob = #{dob}, department_id = #{department.id}, salary = #{salary}, status = #{status}, photo = #{photo} where id = #{id}")
    public int updateEmployee(Employee employee);

    @Delete("delete from employee where id = #{employeeId}")
    public int deleteEmployee(int employeeId);

    @Select("select * from employee where id = #{id}")
    @Results({
            @Result(id=true, property = "id",column = "id"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "nickName", column = "nick_name"),
            @Result(property = "gender", column = "gender"),
            @Result(property = "dob", column = "dob"),
            @Result(property = "department", column = "department_id",one = @One(select="com.skoodeskill.mappers.DepartmentMapper.getDepartmentById")),
            @Result(property = "salary", column = "salary"),
            @Result(property = "status", column = "status"),
            @Result(property = "photo", column = "photo")
    })
    public Employee getEmployeeById(int employeeId);
}
