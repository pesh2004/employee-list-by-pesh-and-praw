package com.skoodeskill.model;

/**
 * Created by PeshZ on 10/8/15.
 */
public class Telephone {
    private int id;
    private int employeeId;
    private String phoneNumber;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public int getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(int employeeId) {
        this.employeeId = employeeId;
    }

    @Override
    public String toString() {
        return "Telephone{" +
                "id=" + id +
                ", employeeId=" + employeeId +
                ", phoneNumber='" + phoneNumber + '\'' +
                '}';
    }
}
