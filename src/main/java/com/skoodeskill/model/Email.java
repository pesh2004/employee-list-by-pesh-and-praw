package com.skoodeskill.model;

/**
 * Created by PeshZ on 10/8/15.
 */
public class Email {

    private int id;
    private int employeeId;
    private String email;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(int employeeId) {
        this.employeeId = employeeId;
    }

    @Override
    public String toString() {
        return "Email{" +
                "id=" + id +
                ", employeeId=" + employeeId +
                ", email='" + email + '\'' +
                '}';
    }
}
