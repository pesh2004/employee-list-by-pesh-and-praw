package com.skoodeskill.services;

import com.skoodeskill.mappers.DepartmentMapper;
import com.skoodeskill.model.Department;
import org.apache.ibatis.session.SqlSession;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import java.util.List;

/**
 * Created by PeshZ on 10/8/15.
 */
@ManagedBean(name = "department")
@ApplicationScoped
public class DepartmentService implements DepartmentMapper {

    @Override
    public Department getDepartmentById(int departmentId) {
        return null;
    }

    @Override
    public List<Department> getAllDepartment() {

        SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();

        try {
            DepartmentMapper departmentMapper = sqlSession.getMapper(DepartmentMapper.class);
            return departmentMapper.getAllDepartment();
        } finally {
            sqlSession.close();
        }
    }
}
