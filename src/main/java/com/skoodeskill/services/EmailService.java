package com.skoodeskill.services;

import com.skoodeskill.mappers.EmailMapper;
import com.skoodeskill.model.Email;
import org.apache.ibatis.session.SqlSession;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import java.util.List;

/**
 * Created by PeshZ on 10/8/15.
 */
@ManagedBean(name = "email")
@ApplicationScoped
public class EmailService implements EmailMapper {

    @Override
    public int insertEmail(Email email) {
        SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
        try {
            EmailMapper emailMapper = sqlSession.getMapper(EmailMapper.class);
            int effectRow = emailMapper.insertEmail(email);
            sqlSession.commit();
            return effectRow;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public List<Email> getEmailByEmployeeId(int employeeId) {
        SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();

        try {
            EmailMapper emailMapper = sqlSession.getMapper(EmailMapper.class);
            return emailMapper.getEmailByEmployeeId(employeeId);
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public int deleteEmailByEmployeeId(int employeeId) {
        SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
        try {
            EmailMapper emailMapper = sqlSession.getMapper(EmailMapper.class);
            int effectRow = emailMapper.deleteEmailByEmployeeId(employeeId);
            sqlSession.commit();
            return effectRow;
        } finally {
            sqlSession.close();
        }
    }
}
