package com.skoodeskill.services;

import com.skoodeskill.mappers.TelephoneMapper;
import com.skoodeskill.model.Telephone;
import org.apache.ibatis.session.SqlSession;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import java.util.List;

/**
 * Created by PeshZ on 10/8/15.
 */
@ManagedBean(name="telephone")
@ApplicationScoped
public class TelephoneService implements TelephoneMapper {
    @Override
    public int insertTelephone(Telephone telephone) {
        SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
        try {
            TelephoneMapper telephoneMapper = sqlSession.getMapper(TelephoneMapper.class);
            int effectRow = telephoneMapper.insertTelephone(telephone);
            sqlSession.commit();
            return effectRow;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public List<Telephone> getTelephoneByEmployeeId(int employeeId) {
        SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();

        try {
            TelephoneMapper telephoneMapper = sqlSession.getMapper(TelephoneMapper.class);
            return telephoneMapper.getTelephoneByEmployeeId(employeeId);
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public int deleteTelephoneByEmployeeId(int employeeId) {
        SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
        try {
            TelephoneMapper telephoneMapper = sqlSession.getMapper(TelephoneMapper.class);
            int effectRow = telephoneMapper.deleteTelephoneByEmployeeId(employeeId);
            sqlSession.commit();
            return effectRow;
        } finally {
            sqlSession.close();
        }
    }
}
