package com.skoodeskill.services;

import com.skoodeskill.mappers.EmployeeMapper;
import com.skoodeskill.model.Employee;
import org.apache.ibatis.session.SqlSession;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by PeshZ on 10/7/15.
 */
@ManagedBean(name = "employee")
@ApplicationScoped
public class EmployeeService implements EmployeeMapper {

    @Override
    public List<Employee> getAllEmployee() {
        SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();

        try {
            EmployeeMapper employeeMapper = sqlSession.getMapper(EmployeeMapper.class);
            return employeeMapper.getAllEmployee();
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public int insertEmployee(Employee employee) {
        SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
        try {
            EmployeeMapper employeeMapper = sqlSession.getMapper(EmployeeMapper.class);
            int effectRow = employeeMapper.insertEmployee(employee);
            sqlSession.commit();
            return effectRow;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public int updateEmployee(Employee employee) {
        SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
        try {
            EmployeeMapper employeeMapper = sqlSession.getMapper(EmployeeMapper.class);
            int effectRow = employeeMapper.updateEmployee(employee);
            sqlSession.commit();
            return effectRow;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public int deleteEmployee(int employeeId) {
        SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
        try {
            EmployeeMapper employeeMapper = sqlSession.getMapper(EmployeeMapper.class);
            int effectRow = employeeMapper.deleteEmployee(employeeId);
            sqlSession.commit();
            return effectRow;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public Employee getEmployeeById(int employeeId) {
        SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();

        try {
            EmployeeMapper employeeMapper = sqlSession.getMapper(EmployeeMapper.class);
            return employeeMapper.getEmployeeById(employeeId);
        } finally {
            sqlSession.close();
        }
    }

    public List<Employee> getAllEmployeeWithAge(){
        List<Employee> employeeLists = getAllEmployee();

        for (Employee employee:employeeLists){
            int age = calcAge(employee.getDob());
            employee.setAge(age);
        }

        return employeeLists;
    }

    public int calcAge(Date dob) {
        int age = 0;
        Calendar dofb = Calendar.getInstance();
        dofb.setTime(dob);
        Calendar today = Calendar.getInstance();
        age = today.get(Calendar.YEAR) - dofb.get(Calendar.YEAR);
        return age;
    }
}
