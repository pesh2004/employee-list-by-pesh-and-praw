package com.skoodeskill.views;

import com.skoodeskill.model.Department;
import com.skoodeskill.model.Email;
import com.skoodeskill.model.Employee;
import com.skoodeskill.model.Telephone;
import com.skoodeskill.services.EmailService;
import com.skoodeskill.services.EmployeeService;
import com.skoodeskill.services.TelephoneService;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import java.io.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * Created by PeshZ on 10/8/15.
 */
@ManagedBean(name="employeeView")
@ViewScoped
public class EmployeeView implements Serializable {
    private List<Employee> employees;
    private List<Employee> employeesFilter;
    private Employee employeeEntity;
    private List<Telephone> telephoneList = new ArrayList<Telephone>();
    private List<Email> emailList = new ArrayList<Email>();
    private int age;
    private String status;
    private String styleStatus;
    private String salary;
    private String employeeId;
    private UploadedFile file;
    private String photo;
    private Date currentDate = new Date();


    public List<Email> getEmailList() {
        return emailList;
    }

    public void setEmailList(List<Email> emailList) {
        this.emailList = emailList;
    }

    private DepartmentView departmentView;

    @ManagedProperty("#{employee}")
    private EmployeeService employeeService;

    @ManagedProperty("#{telephone}")
    private TelephoneService telephoneService;

    @ManagedProperty("#{email}")
    private EmailService emailService;

    public EmployeeService getEmployeeService() {
        return employeeService;
    }

    public EmailService getEmailService() {
        return emailService;
    }

    public void setEmailService(EmailService emailService) {
        this.emailService = emailService;
    }

    @PostConstruct
    public void init() {
        employees = employeeService.getAllEmployeeWithAge();
        setStatus("Not Hired");
        setStyleStatus("#FF0000");
        setSalary(null);
    }

    public DepartmentView getDepartmentView() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return  (DepartmentView) facesContext.getApplication().getVariableResolver().resolveVariable(facesContext, "departmentView");
    }

    public void startForm(){
        ServletContext servletContext = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
        String resourcesPath = servletContext.getRealPath("/resources/");
        System.out.println(resourcesPath);

        FacesContext context = FacesContext.getCurrentInstance();
        employeeId = context.getExternalContext().getRequestParameterMap().get("employeeId");
        departmentView = getDepartmentView();
        if (employeeId == null){
            employeeEntity = new Employee();
            employeeEntity.setGender("male");
            employeeEntity.setTelephone(new Telephone());
            employeeEntity.setEmail(new Email());
            employeeEntity.setPhoto("personal.png");
        }else{
            employeeEntity = employeeService.getEmployeeById(Integer.parseInt(employeeId));
            employeeEntity.setTelephone(new Telephone());
            employeeEntity.setEmail(new Email());
            departmentView.setSelectDepartment(employeeEntity.getDepartment().getId());
            setSalary(String.valueOf(employeeEntity.getSalary()));
            telephoneList = telephoneService.getTelephoneByEmployeeId(Integer.parseInt(employeeId));
            emailList = emailService.getEmailByEmployeeId(Integer.parseInt(employeeId));
            if (employeeEntity.getStatus().toLowerCase().equals("hired")){
                setStatus("Hired");
                setStyleStatus("green");
                employeeEntity.setStatus("true");
            }
            if (employeeEntity.getPhoto() == null){
                employeeEntity.setPhoto("personal.png");
            }
        }
        System.out.println(employeeEntity.getEmail().getEmail());
        System.out.println("start form");
    }

    public List<Employee> getEmployees() {
        return employees;
    }

    public void setEmployees(List<Employee> employees) {
        this.employees = employees;
    }

    public void setEmployeeService(EmployeeService employeeService) {
        this.employeeService = employeeService;
    }

    public List<Employee> getEmployeesFilter() {
        return employeesFilter;
    }

    public void setEmployeesFilter(List<Employee> employeesFilter) {
        this.employeesFilter = employeesFilter;
    }

    public Employee getEmployeeEntity() {
        return employeeEntity;
    }

    public void setEmployeeEntity(Employee employeeEntity) {
        this.employeeEntity = employeeEntity;
    }

    public void calculateAge(){
        int age = employeeService.calcAge(employeeEntity.getDob());
        setAge(age);
        System.out.println(getAge());
    }

    public void activeHired(){
        System.out.println(employeeEntity.getStatus());
        if (employeeEntity.getStatus() == "true"){
            setStatus("Hired");
            setStyleStatus("green");
        }else {
            setStatus("Not Hired");
            setStyleStatus("#FF0000");
        }
        System.out.println(getStatus());
    }

    public void startAddTel(){
        employeeEntity.getTelephone().setPhoneNumber(null);
    }

    public void startAddEmail(){
        employeeEntity.getEmail().setEmail(null);
    }

    public void addTel(){
        Telephone telephone = new Telephone();
        telephone.setPhoneNumber(employeeEntity.getTelephone().getPhoneNumber());
        telephoneList.add(telephone);
        RequestContext.getCurrentInstance().execute("PF('dlgTel').hide()");
    }

    public void addEmail(){
        Email email = new Email();
        email.setEmail(employeeEntity.getEmail().getEmail());
        emailList.add(email);
        RequestContext.getCurrentInstance().execute("PF('dlgEmail').hide()");
    }

    public void saveData(){
        String redirect = "";
        if (telephoneList.size() > 0 && emailList.size() > 0){
            Employee employee = new Employee();
            Department department = new Department();
            employee.setId(employeeEntity.getId());
            employee.setFirstName(employeeEntity.getFirstName());
            employee.setLastName(employeeEntity.getLastName());
            employee.setNickName(employeeEntity.getNickName());
            employee.setGender(employeeEntity.getGender());
            employee.setDob(employeeEntity.getDob());
            department.setId(departmentView.getSelectDepartment());
            employee.setDepartment(department);
            employee.setSalary(Integer.parseInt(getSalary()));
            employee.setPhoto(null);
            String statusHired = null;
            if (employeeEntity.getStatus() == "true"){
                statusHired = "Hired";
            }else{
                statusHired = "not-hired";
            }
            employee.setStatus(statusHired);

            ServletContext servletContext = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
            String resourcesPath = servletContext.getRealPath("/resources/");

            try {
                UUID idOne = UUID.randomUUID();
                String filename = idOne + ".jpg";
                InputStream input = file.getInputstream();
                OutputStream output = new FileOutputStream(new File(resourcesPath, filename));
                int read = 0;
                byte[] bytes = new byte[1024];

                while ((read = input.read(bytes)) != -1) {
                    output.write(bytes, 0, read);
                }
                input.close();
//                output.flush();
                output.close();
                employee.setPhoto(filename);
            } catch (Exception e) {
                System.out.println(e);
            }

            try {
                int effectRow;
                employeeId = String.valueOf(employeeEntity.getId());
                if (employeeId.equals("0")){
                    System.out.println("insert");
                    effectRow = employeeService.insertEmployee(employee);
                    for (Telephone telephone:telephoneList){
                        telephone.setEmployeeId(employee.getId());
                        effectRow = telephoneService.insertTelephone(telephone);
                    }

                    for (Email email:emailList){
                        email.setEmployeeId(employee.getId());
                        effectRow = emailService.insertEmail(email);
                    }
                }else{
                    System.out.println("update");
                    effectRow = employeeService.updateEmployee(employee);
                    telephoneService.deleteTelephoneByEmployeeId(employee.getId());
                    for (Telephone telephone:telephoneList){
                        telephone.setEmployeeId(employee.getId());
                        effectRow = telephoneService.insertTelephone(telephone);
                    }

                    emailService.deleteEmailByEmployeeId(employee.getId());
                    for (Email email:emailList){
                        email.setEmployeeId(employee.getId());
                        effectRow = emailService.insertEmail(email);
                    }
                }

                redirect = "index.xhtml";
                FacesContext.getCurrentInstance().getExternalContext().redirect(redirect);
            } catch (Exception e) {
                System.out.println(e);
            }
        }else{
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "มีข้อผิดพลาด!", "กรุณากรอกข้อมูลเบอร์โทรศัพท์ และ Email ให้ครบ"));
        }
    }

    public void uploadEvent(FileUploadEvent event){
        file = event.getFile();
        System.out.println(file);
    }

    public String deleteEmployee(int employeeId){
        employeeService.deleteEmployee(employeeId);
        return "index.xhtml?faces-redirect=true";
    }

    public void setDepartmentView(DepartmentView departmentView) {
        this.departmentView = departmentView;
    }

    public List<Telephone> getTelephoneList() {
        return telephoneList;
    }

    public void setTelephoneList(List<Telephone> telephoneList) {
        this.telephoneList = telephoneList;
    }

    public TelephoneService getTelephoneService() {
        return telephoneService;
    }

    public void setTelephoneService(TelephoneService telephoneService) {
        this.telephoneService = telephoneService;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStyleStatus() {
        return styleStatus;
    }

    public void setStyleStatus(String styleStatus) {
        this.styleStatus = styleStatus;
    }

    public String getSalary() {
        return salary;
    }

    public void setSalary(String salary) {
        this.salary = salary;
    }

    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    public UploadedFile getFile() {
        return file;
    }

    public void setFile(UploadedFile file) {
        this.file = file;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public Date getCurrentDate() {
        return currentDate;
    }

    public void setCurrentDate(Date currentDate) {
        this.currentDate = currentDate;
    }
}
