package com.skoodeskill.views;

import com.skoodeskill.model.Department;
import com.skoodeskill.services.DepartmentService;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import java.io.Serializable;
import java.util.List;

/**
 * Created by PeshZ on 10/8/15.
 */
@ManagedBean(name="departmentView")
@ViewScoped
public class DepartmentView implements Serializable {
    @ManagedProperty("#{department}")
    private DepartmentService departmentService;

    private List<Department> departments;

    private int selectDepartment;

    @PostConstruct
    public void init() {
        departments = departmentService.getAllDepartment();
        selectDepartment = 0;
    }

    public void setDepartmentService(DepartmentService departmentService) {
        this.departmentService = departmentService;
    }

    public List<Department> getDepartments() {
        return departments;
    }

    public void setDepartments(List<Department> departments) {
        this.departments = departments;
    }

    public int getSelectDepartment() {
        return selectDepartment;
    }

    public void setSelectDepartment(int selectDepartment) {
        this.selectDepartment = selectDepartment;
    }
}
